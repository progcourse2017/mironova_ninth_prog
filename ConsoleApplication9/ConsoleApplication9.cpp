// ConsoleApplication9.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include <clocale>
#include <string>
#include <vector>
using namespace std;

class petshop
{
public:
	petshop(); 
	petshop(int , char *, bool , char *, int ); 
	petshop(const petshop&); 

	char*GetName();  
	char*GetAnimal();  
	int Getquantity();   
	int Getprice();   
	bool GetSex();   
	void Show();          
	void SetName(char* _name);  
	void SetAnimal(char* _animal);  
	void Setquantity(int _quantity);   
	void Setprice(int _price);    
	void SetSex(bool _sex);    
	void Set(int _quantity, char *_animal, bool _sex, char *_name, int _price);  
	~petshop();
private:
	int quantity;
	char *animal;
	bool sex;
	char *name;
	int price;
	
	
};
petshop::petshop()        
{
	quantity = 0;
	animal = " ";
	sex = " ";
	name = " ";
	 price = 0;
	

};
petshop::petshop(int _quantity, char *_animal, bool _sex, char *_name, int _price) 
{   quantity = _quantity;
    animal = _animal;
    sex = _sex;
	name = _name;
	price = _price;
	if (_sex == 0)
	{
		cout << "Мужской пол" << endl;
	}
	else
	{
		cout << "Женский пол" << endl;
	}

};
petshop::petshop(const petshop &b)   
{
	quantity = b.quantity;
    animal = b.animal;
    sex = b.sex;
	name = b.name;
	price = b.price;

	
}
petshop::~petshop()
{

	quantity = 0;
	animal = " ";
	sex = " ";
	name = " ";
	price = 0;
	
};

char* petshop::GetName() { return name; }; 
char*petshop::GetAnimal() { return animal; }; 
int petshop::Getquantity() { return quantity; };
int petshop::Getprice() { return price; }; 
bool petshop::GetSex() { return sex; };
void petshop::Show()                 
{
	cout << "Количество животных: " << quantity << endl;
	cout << "Вид животного: " << animal << endl;
	cout << "Пол: " << sex << endl;
	cout << "Имя: " << name << endl;
	cout << "Цена: " << price << endl;
	
};
void petshop::Setquantity(int _quantity) { quantity = _quantity; };  
void petshop::SetAnimal(char *_animal) { animal =_animal; };   
void petshop::SetSex(bool _sex) { sex = _sex; };    
void petshop::SetName(char *_name) { name = _name; };  
void petshop::Setprice(int _price) { price = _price; }; 
void petshop::Set(int _quantity,char *_animal, bool _sex, char *_name, int _price )
{
	cout << "Количество животных: " << endl;
	
	quantity = _quantity;
	cin >> quantity;
	cout << "Вид животного: " << endl;
	
	animal = _animal;
	cin >> animal;
	cout << "Пол(0-мужской,1-женский): " << endl;
	
	sex = _sex;
	cin >>sex;
	cout << "Имя: " << endl;
	
	name = _name;
	cin >> name;
	cout << "Цена: " << endl;
	
	price = _price;
	cin >> price;
};
int const SIZE = 30;

    int quantity;
	char *animal = (char*)malloc(SIZE);
	bool sex;
	char *name = (char*)malloc(SIZE);
	int price;

	int main(int argc, char **argv) {
		short select;
		short i = -1;
		petshop obj[30];
	

	setlocale(LC_ALL, "rus");
	
	cout << "Hello !! \n\n";
	do
	{
		cout << "1: Add Object \n" << "2: look objects\n" << "3: Modify  \n" << "4: Exit \n\n";
		do
		{
			cout << "your choose:   ";
			cin >> select;
			if (select < 1 || select > 4)
				cout << "Wrong choose...Try again \n" << endl;
		} while (select < 1 || select > 4);
		switch (select)
		{
		case 1:
			i++;
			obj[i].Set(quantity, animal, sex, name, price);
			break;

		case 2:
			if (i == -1)
			{
				cout << "There is nothing to show..." << endl;
				break;
			}
			for (short j = 0; j <= i; j++)
			{
				obj[j].Show();


			}
			system("pause");
			break;
		case 3:

			int mod_select;
			int i;
			cout << "choose what object you want to modify:   "; cin >> mod_select;
			do {
				cout << "choose what  you want to modify:(quantity(1), animal(2), sex(3), name(4), price(5)) "; cin >> i;
				if (i < 1 || i> 5)
					cout << "Wrong choose...Try again \n" << endl;
			} while (i < 1 || i > 5);
			switch (i)
			{
			case 1:
				cin >> quantity;
				obj[mod_select].Setquantity(quantity);
				break;

			case 2:
				cin >> animal;
				obj[mod_select].SetAnimal(animal);
				break;
			case 3:
				cin >> sex;
				obj[mod_select].SetSex(sex);
				break;
			case 4:
				cin >> name;
				obj[mod_select].SetName(name);
				break;
			case 5:
				cin >> price;
				obj[mod_select].Setprice(price);
				break;

			default:
				cout << "Error!\n";
			}
			cout << "After the changes, the object looks like this:" << "\n";
			obj[mod_select].Show();
			system("pause");

		case 4:
			cout << "Good luck..";
			break;
		}
		system("cls");
	} while (select != 4);

system("pause");

	return 0;
	}

