#include "stdafx.h"
#include<iostream>
const int SIZE = 100;
using namespace std;

class pet_shop {
private:
	char animal;
	char* sex;
	char* name;
	int price;
	int number_of_animals;
public:
	pet_shop();
	pet_shop(char* _animal, char* _sex, char* _name, int _price, int _number_of_animals);
	pet_shop(const pet_shop & pet_shop0);
	~pet_shop() {};
	void setDate(char*_animal, char* _sex, char* _name, int _price, int _number_of_animals);
void getDate();
void purchase();
};